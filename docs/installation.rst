.. installation:

Installation
============

The collection is available on `Ansible Galaxy`__, a central database of Ansible playbooks, roles and plugins.

The project is `published there as an Ansible collection`__. To install it on your Ansible controller, you have to use the :command:`ansible-galaxy` command provided with Ansible.

.. __: https://galaxy.ansible.com/
.. __: https://galaxy.ansible.com/julien_lecomte/proxmox

To install the Proxmox collection, run the command:

.. code-block:: bash

   ansible-galaxy collection install julien_lecomte.proxmox

You can also use a :file:`requirements.yml` at the root of your repository:

.. code-block:: yaml

   ---
   collections:
     - name: julien_lecomte.proxmox

After saving the file, you can use the command:

.. code-block:: bash

   ansible-galaxy install -r requirements.yml

The collection will then be made available in the directory :file:`~/.ansible/collections/ansible_collections/julien_lecomte/proxmox/`

Upgrading
~~~~~~~~~

To upgrade the already installed collection to the latest release published on Ansible Galaxy, you can run the command:

.. code-block:: bash

   ansible-galaxy collection install --upgrade julien_lecomte.proxmox

Development
~~~~~~~~~~~

If you wish to use the git repository directly, for development or not, change your :file:`ansible.cfg` so that the git repository is in `collections_paths` before other sources.

.. code-block:: ini

   [defaults]
   collections_paths = ~/src/jlecomte/ansible/ansible-proxmox-collection:~/.ansible/collections

.. seealso::

  `Ansible Galaxy - Using collections <https://docs.ansible.com/ansible/devel/user_guide/collections_using.html#collections>`__
    Detailed information about installing and using collections from Galaxy.
