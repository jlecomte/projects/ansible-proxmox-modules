Modules
=======

Multiple Proxmox module plugins are installed with this collection.

They are automatically used by the playbook and role, but can also be used independently in your own roles.

.. toctree::
   :maxdepth: 1
   :caption: Modules

   modules/julien_lecomte.proxmox.acl_module.rst
   modules/julien_lecomte.proxmox.group_module.rst
   modules/julien_lecomte.proxmox.pool_module.rst
   modules/julien_lecomte.proxmox.realm_module.rst
   modules/julien_lecomte.proxmox.role_module.rst
   modules/julien_lecomte.proxmox.token_module.rst
   modules/julien_lecomte.proxmox.user_module.rst
