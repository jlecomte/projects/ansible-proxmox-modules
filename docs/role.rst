
Role
====

Usage
-----

The provided playbook uses the provided role to configure Proxmox on your hosts.

The role itself uses this collection's modules in a specific order. These tasks will only run for hosts that have a variable ``proxmox__enabled`` set to *True*, and has configuration variables set for the task. If using the role via the playbook, host must also be part of group ``debops_service_proxmox``.

There is a one-to-one correspondence between role tasks and Proxmox modules.

The configuration variables for a task are under the form ``proxmox__NAME``, ``proxmox__group_NAME`` and ``proxmox__host_NAME`` where NAME is the plural name of the module. For example, to configure Proxmox realms, you can use ``proxmox__host_realms`` for realms that are specific to the host.

The configuration variables allow configuration at the host level, as a group level, or as a global level. The three variables will be combined by the role.

The variables have the same keys as the modules of the same name.

Example
-------

Let's imagine that we want our host *server.example.com* to have an Active Directory realm. We'll thus use the variable ``proxmox__host_realms`` and the keys described in :ref:`realm module <ansible_collections.julien_lecomte.proxmox.realm_module>`.

This example is in the  `Ansible Roster inventory`__ format.

.. __: https://gitlab.com/jlecomte/projects/ansible-roster

.. code-block:: yaml

   ---
   plugin: roster

   hosts:
     server.example.com:
       groups:
         - debops_service_proxmox
       vars:
         proxmox__enabled: True
         proxmox__host_realms:
           - name: corp
             type: ad
             domain: corp.example.com
             server1: corp.example.com
             default: true
             secure: true
             comment: "Example Active Directory (corp.example.com)"
