-r requirements.txt

ansible
antsibull-docs
pylint
pytest
pytest-cov
pytest-html
pytest-mock
pytest-order
sphinx
sphinx-rtd-theme
sphinxcontrib-htmlhelp
sphinxcontrib-napoleon
tomli
