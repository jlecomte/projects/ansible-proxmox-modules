import json

from ansible.module_utils import basic
from ansible.module_utils._text import to_bytes
from pytest import fixture, raises

from ansible_collections.julien_lecomte.proxmox.plugins.module_utils.pvesh import ProxmoxShell


class MockProxmoxShell:
    def __init__(self, mocker):
        self.commands = []  # expected commands
        self.output = {}

        mocker.patch.object(ProxmoxShell, "execute_command", self.execute_command)
        mocker.patch.object(ProxmoxShell, "fail_json", self.fail_json)
        mocker.patch.object(ProxmoxShell, "exit_json", self.exit_json)

    def set_params(self, params):
        args = {"ANSIBLE_MODULE_ARGS": params, "_ansible_remote_tmp": "/tmp", "_ansible_keep_remote_files": False}
        args = json.dumps(args)
        args = to_bytes(args)
        # pylint: disable=protected-access
        basic._ANSIBLE_ARGS = args
        return args

    def execute_command(self, cmd, **_kwargs):
        cmd = (" ".join(cmd)).strip()

        assert len(self.commands) != 0, f'Error: No mock commands left in expected stack, but module called "{cmd}".'
        command = self.commands.pop(0)
        expected_cmd = command[0].strip()
        assert expected_cmd == cmd, f'Error: Expected command to be "{expected_cmd}" but got "{cmd}" instead.'

        # if stdout is None, make it invalid json
        # it represents item does not exist
        stdout = command[1]
        if stdout is None:
            return 99, "", "error: nothing"

        # return exitcode, stdout, stderr
        return 0, json.dumps(stdout), ""

    def fail_json(self, *_args, **kwargs):
        assert kwargs == self.output
        raise SystemExit

    def exit_json(self, *_args, **kwargs):
        for k, v in self.output.items():
            assert k in kwargs, f"Incorrect module output. Field '{k}' is missing."
            assert (
                v == kwargs[k]
            ), f"Incorrect module output. Field '{k}' has value '{v}' but was expected to be '{kwargs[k]}'."
        raise SystemExit

    def run(self, params, commands, output, main):
        self.set_params(params)
        self.commands = commands
        self.output = output
        with raises(SystemExit):
            main()
        assert len(self.commands) == 0, f"Error: {len(self.commands)} unused command line arguments."


@fixture(scope="function")
def plugin(mocker):
    return MockProxmoxShell(mocker)
