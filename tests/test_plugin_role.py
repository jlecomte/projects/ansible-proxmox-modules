from pytest import mark

from ansible_collections.julien_lecomte.proxmox.plugins.modules import role


@mark.parametrize(
    "params, commands, output",
    # fmt: off
    [
      ( #-----------------------------------------------------------------------
        # 1. absent to absent
        {"name": "example", "state": "absent"},
        [
            ("pvesh get /access/roles/example --output-format=json", None),
        ],
        { "changed": False},
      ),
      ( #-----------------------------------------------------------------------
        # 2. absent to present
        {"name": "example", "state": "present"},
        [
            ("pvesh get /access/roles/example --output-format=json", None),
            ("pvesh create /access/roles -roleid example --privs", {}),
            ("pvesh get /access/roles/example --output-format=json", {"roleid": "example", "privs": ""}),
        ],
        { "changed": True},
      ),
      ( #-----------------------------------------------------------------------
        # 3. present to present
        {"name": "example", "state": "present"},
        [
            ("pvesh get /access/roles/example --output-format=json", {"roleid": "example", "privs": ""}),
            ("pvesh set /access/roles/example --privs", {}),
            ("pvesh get /access/roles/example --output-format=json", {"roleid": "example", "privs": ""}),
        ],
        { "changed": True}, # FIXME
      ),
      ( #-----------------------------------------------------------------------
        # 4. present to absent
        {"name": "example", "state": "absent"},
        [
            ("pvesh get /access/roles/example --output-format=json", {"roleid": "example", "privs": ""}),
            ("pvesh delete /access/roles/example", {}),
            ("pvesh get /access/roles/example --output-format=json", None),
        ],
        { "changed": True},
      ),
      ( #-----------------------------------------------------------------------
       # Set roles, no append
        {"name": "example", "state": "present", "privs": ["TstPriv"]},
        [
            ("pvesh get /access/roles/example --output-format=json", {"roleid": "example", "privs": ""}),
            ("pvesh set /access/roles/example --privs TstPriv", ""),
            ("pvesh get /access/roles/example --output-format=json", {"roleid": "example", "privs": "TstPriv"}),
        ],
        { "changed": True},
      ),
#      ( #-----------------------------------------------------------------------
#        # Replace role but it already is correct
#        {"name": "example", "state": "present", "privs": ["TstPriv"]},
#        [
#            ("pvesh get /access/roles/example --output-format=json", {"roleid": "example", "privs": "TstPriv"}),
#            ("pvesh set /access/roles/example --privs TstPriv", {"roleid": "example", "privs": "TstPriv"}),
#            ("pvesh get /access/roles/example --output-format=json", {"roleid": "example", "privs": "TstPriv"}),
#        ],
#        { "changed": False},
#      ),
      ( #-----------------------------------------------------------------------
        # Set roles, append
        {"name": "example", "state": "present", "privs": ["TstPriv"], "append": True},
        [
            ("pvesh get /access/roles/example --output-format=json", {"roleid": "example", "privs": "OtherPriv"}),
            ("pvesh set /access/roles/example -append 1 --privs TstPriv", {}),
            ("pvesh get /access/roles/example --output-format=json", {}),
        ],
        { "changed": True},
      ),
    ],
    # fmt: on
)
@mark.order(2)
def test_plugin_role(plugin, params, commands, output):
    plugin.run(params, commands, output, role.main)
