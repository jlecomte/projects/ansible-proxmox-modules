[![documentation](https://img.shields.io/badge/documentation-html-informational)](https://jlecomte.gitlab.io/ansible/ansible-proxmox-collection)
[![license](https://img.shields.io/badge/license-GPL%203.0%20only-brightgreen)](https://spdx.org/licenses/GPL-3.0-only.html)
[![pipelines](https://gitlab.com/jlecomte/ansible/ansible-proxmox-collection/badges/master/pipeline.svg)](https://gitlab.com/jlecomte/ansible/ansible-proxmox-collection/pipelines)
[![coverage](https://gitlab.com/jlecomte/ansible/ansible-proxmox-collection/badges/master/coverage.svg)](https://jlecomte.gitlab.io/ansible/ansible-proxmox-collection/coverage/index.html)

# Ansible Proxmox collection

Ansible collection that contains a Proxmox VE playbook, a role and a module plugins that do not require an API key. DebOps compatible.

Modules include acl, group, pool, realm, role, user token and user.

## Installation from Ansible Galaxy

You can install the latest version from Ansible Galaxy:

~~~bash
ansible-galaxy collection install -U julien_lecomte.proxmox
~~~

## Usage

Please refer to the full documentation for usage.

## License

This project is licensed under the GPL-3.0-only License - see the [LICENSE](LICENSE) file for details.

## Locations

  * Documentation: [https://jlecomte.gitlab.io/ansible/ansible-proxmox-collection/](https://jlecomte.gitlab.io/ansible/ansible-proxmox-collection)
  * GitLab: [https://gitlab.com/jlecomte/ansible/ansible-proxmox-collection](https://gitlab.com/jlecomte/ansible/ansible-proxmox-collection)
  * Galaxy: [https://galaxy.ansible.com/julien_lecomte/proxmox](https://galaxy.ansible.com/julien_lecomte/proxmox)
