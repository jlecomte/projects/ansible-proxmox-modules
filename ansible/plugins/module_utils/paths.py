# Copyright (C) 2022 Julien Lecomte <julien@lecomte.at>
# SPDX-License-Identifier: GPL-3.0-only

import json

from ansible.module_utils.basic import AnsibleModule


class NodePath(AnsibleModule):
    def set_node_name(self):
        if self.params["node"] not in [None, "", "localhost"]:
            return
        with open("/etc/pve/.members", encoding="UTF-8") as fd:
            data = json.load(fd)
        self.params["node"] = data["nodename"]
