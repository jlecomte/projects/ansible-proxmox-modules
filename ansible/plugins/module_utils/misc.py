# Copyright (C) 2022 Julien Lecomte <julien@lecomte.at>
# SPDX-License-Identifier: GPL-3.0-only
#
def split_into_list(value):
    if value:
        return sorted(value.split(","))
    return []
