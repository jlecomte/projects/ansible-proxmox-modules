#!/usr/bin/env python3
# Copyright (C) 2022 Julien Lecomte <julien@lecomte.at>
# SPDX-License-Identifier: GPL-3.0-only

import os
import tempfile

from ansible_collections.julien_lecomte.proxmox.plugins.module_utils.paths import NodePath
from ansible_collections.julien_lecomte.proxmox.plugins.module_utils.pvesh import ProxmoxShell

DOCUMENTATION = r"""
---
module: certificate
plugin_type: module
author: Julien Lecomte (julien@lecomte.at)
short_description: Adds, modifies, or removes a SSL certificate
description:
  - Adds, modifies, or removes a SSL certificate.
  - Returned values will exist in a variable named 'certificate'.
seealso:
  - name: PVE API
    description: Proxmox VE Application Programming Interface
    link: https://pve.proxmox.com/pve-docs/api-viewer/#/nodes/{node}/certificates/custom
options:
  node:
    description:
      - The cluster node name (or empty/"localhost" for localhost).
    type: str
  certificate:
    description:
      - PEM encoded certificate (chain).
    type: str
  state:
    description:
      - Specify if the certificate should exist (present) or absent.
    type: str
    choices: [ absent, present ]
    default: present
  key:
    description:
      - PEM encoded private key.
    type: str
  restart:
    description:
      - Restart pveproxy (default True).
    type: boolean
"""

EXAMPLES = r"""
- name: Add or modify certificate
  julien_lecomte.proxmox.certificate:
    certificate: '{{ lookup("ansible.builtin.file", "public1.pem") }}'
    key: '{{ lookup("ansible.builtin.file", "private1.pem") }}'
    state: "present"

- name: Clean up
  julien_lecomte.proxmox.certificate:
    state: "absent"
"""

RETURN = r"""
state:
  description: State ("absent" or "present")
  returned: always
  type: str
"""


class ProxmoxCertificate(ProxmoxShell, NodePath):
    def on_detect_action(self):
        if self.params["state"] == "present" and self.data["state"] == "absent":
            self.do_create_cmdline()
            self.changed = True
        elif self.params["state"] == "present" and self.data["state"] == "present":
            self.do_modify_cmdline()
        elif self.params["state"] == "absent" and self.data["state"] == "absent":
            return
        elif self.params["state"] == "absent" and self.data["state"] == "present":
            self.do_delete_cmdline()
            self.changed = True

    def on_load(self):
        # We're an array of certificates, so find ourself
        for certificate in self.data:
            if "pveproxy-ssl.pem" == certificate["filename"]:
                self.data = certificate
                return

        raise LookupError()

    def on_create_cmdline(self, cmd):
        cmd.extend(["-key", self.key])
        cmd.extend(self.maybe_command("certificate", "certificates"))
        cmd.extend(self.maybe_command("restart"))

        # A certificate might already exist since modify is actually create, so force:
        cmd.extend(["-force", "1"])
        self.changed = True
        return cmd

    def on_modify_cmdline(self, cmd):
        # We need to compare certificates
        os.environ["LANG"] = "C"
        with tempfile.NamedTemporaryFile() as tmp:
            with open(tmp.name, "w", encoding="UTF-8") as fd:
                fd.write(self.params["certificate"])
            openssl = f"openssl x509 -noout -fingerprint -sha256 -inform pem -in {tmp.name}"
            _, stdout, _ = self.execute_command(openssl)
        stdout = stdout[stdout.find("=") + 1 :].strip()

        if stdout == self.data["fingerprint"]:
            return []
        else:
            return self.do_create_cmdline()

    def on_delete_cmdline(self, cmd):
        cmd.extend(self.maybe_command("restart"))
        return cmd


def main():
    module = ProxmoxCertificate(
        argument_spec=dict(
            certificate=dict(type="str"),
            node=dict(type="str"),
            state=dict(type="str", default="present", choices=["absent", "present"]),
            key=dict(type="str"),
            restart=dict(type="bool", default=True),
        ),
        supports_check_mode=False,
    )
    # Remove from params to avoid printing to console
    module.key = module.params.pop("key", None)

    module.set_node_name()
    module.run(
        f"/nodes/{module.params['node']}/certificates/info",
        f"/nodes/{module.params['node']}/certificates/custom",
        f"/nodes/{module.params['node']}/certificates/custom",
    )
    module.exit_json(changed=module.changed, warnings=module.warnings, certificate=module.data)


if __name__ == "__main__":
    main()
