#!/usr/bin/env python3
# Copyright (C) 2022 Julien Lecomte <julien@lecomte.at>
# SPDX-License-Identifier: GPL-3.0-only

from ansible_collections.julien_lecomte.proxmox.plugins.module_utils.pvesh import ProxmoxShell

# TODO: Should we split up and use suboptions
# for things like tfa_type, sync_defaults_options,
# sync_attributes, remove_vanished
DOCUMENTATION = r"""
---
module: realm
plugin_type: module
author: Julien Lecomte (julien@lecomte.at)
short_description: Adds, modifies, or removes a Proxmox realm.
description:
  - Adds, modifies, or removes a Proxmox realm.
  - Returned values will exist in a variable named 'realm'.
seealso:
  - name: PVE API
    description: Proxmox VE Application Programming Interface
    link: https://pve.proxmox.com/pve-docs/api-viewer/#/access/domains
options:
  name:
    description:
      - The realm name.
    type: str
    required: true
  state:
    description:
      - Specify if the realm should exist (present) or absent.
    type: str
    choices: [ absent, present ]
    default: present
  acr_values:
    description:
      - Specifies the Authentication Context Class Reference values that theAuthorization Server is being requested to use for the Auth Request.
    type: str
  autocreate:
    description:
      - Automatically create users if they do not exist.
    type: bool
  base_dn:
    description:
      - LDAP base domain name.
      - Required when type is 'ldap'.
    type: str
  bind_dn:
    description:
      - LDAP bind domain name.
    type: str
  capath:
    description:
      - Path to the CA certificate store.
    type: str
  case_sensitive:
    description:
      - Username is case-sensitive.
    type: bool
  cert:
    description:
      - Path to the client certificate.
    type: str
  certkey:
    description:
      - Path to the client certificate key.
    type: str
  client_id:
    description:
      - OpenID Client ID.
      - Required when type is 'openid'.
    type: str
  client_key:
    description:
      - OpenID Client Key.
    type: str
  comment:
    description:
      - Optionally sets the comment field.
    type: str
  default:
    description:
      - Use this as default realm.
    type: bool
  domain:
    description:
      - AD domain name.
      - Required when type is 'ad'.
    type: str
  filter:
    description:
      - LDAP filter for user sync.
    type: str
  group_classes:
    description:
      - The objectclasses for groups.
    type: str
  group_dn:
    description:
      - LDAP base domain name for group sync.
      - If not set, the base_dn will be used.
    type: str
  group_filter:
    description:
      - LDAP filter for group sync.
    type: str
  group_name_attr:
    description:
      - LDAP attribute representing a groups name.
      - If not set or found, the first value of the DN will be used as name.
    type: str
  issuer_url:
    description:
      - OpenID Issuer URL.
      - Required when type is 'openid'.
    type: str
  mode:
    description:
      - LDAP protocol mode.
    type: str
    choices: [ ldap, ldap+starttls, ldaps ]
  password:
    description:
      - "LDAP bind password. Will be stored in /etc/pve/priv/realm/<REALM>.pw."
    type: str
  port:
    description:
      - Server port.
    type: int
  prompt:
    description:
      - 'Specifies whether the Authorization Server prompts the End-User for reauthentication and consent. (?:none|login|consent|select_account|\S+)'
    type: str
  scopes:
    description:
      - "B(Warning:) not to be confused with parameter I(scope) (singular)."
      - "Specifies the scopes (user details) that should be authorized and returned, for example email or profile."
    type: str
  secure:
    description:
      - Use ssl.
    type: bool
  server1:
    description:
      - Server IP address (or DNS name).
      - Required when type is 'ad' or 'ldap'.
    type: str
  server2:
    description:
      - Fallback Server IP address (or DNS name).
    type: str
  sslversion:
    description:
      - "LDAPS TLS/SSL version. It's not recommended to use version older than 1.2!"
    type: str
    choices: [ tlsv1, tlsv1_1, tlsv1_2, tlsv1_3 ]
  sync:
    description:
      - 'For a sync command to automatically sync users and groups for LDAP-based realms (LDAP & Microsoft Active Directory only)'
      - 'This requires the correct fields to be set.'
      - 'Please refer to the syncing sections of `pveum man page <https://pve.proxmox.com/pve-docs/pveum.1.html>`__.'
    type: bool
    default: false
  sync_options:
    description:
      - "Optional fields for when I(sync) is true"
    type: dict
    suboptions:
      dry_run:
        description:
          - "Only when I(sync) is true."
          - "If set, does not write anything."
        type: bool
        default: False
      enable_new:
        description:
          - "Only when I(sync) is true."
          - Enable newly synced users immediately.
          - "Defaults to true unless set in I(sync_defaults_options)."
        type: bool
      full:
        description:
          - "Only when I(sync) is true."
          - 'B(Deprecated since Proxmox 7.2)'
          - 'If set, uses the LDAP Directory as source of truth, deleting users or groups not returned from the sync and removing all locally modified properties of synced users.'
          - 'If not set, only syncs information which is present in the synced data, and does not delete or modify anything else.'
          - 'Must either be specified in task, or set with I(sync_defaults_options).'
        type: bool
      purge:
        description:
          - "Only when I(sync) is true."
          - 'B(Deprecated since Proxmox 7.2)'
          - 'Remove ACLs for users or groups which were removed from the config during a sync.'
          - 'Must either be specified in task, or set with I(sync_defaults_options).'
        type: bool
      remove_vanished:
        description:
          - "Only when I(sync) is true."
          - 'B(New since Proxmox 7.2)'
          - 'A semicolon-separated list of things to remove when they or the user vanishes during a sync.'
          - 'The following values are possible: C(entry) removes the user/group when not returned from the sync.'
          - 'C(properties) removes the set properties on existing user/group that do not appear in the source (even custom ones).'
          - 'C(acl) removes acls when the user/group is not returned from the sync.'
          - 'Must either be specified in task, or set with I(sync_defaults_options).'
        type: str
      scope:
        description:
          - "B(Warning:) not to be confused with parameter I(scopes) (plural)."
          - "Select what to sync."
        choices: [ both, groups, users ]
  type:
    description:
      - Realm type.
    choices: [ ad, ldap, openid, pam, pve ]
    type: str
  user_attr:
    description:
      - LDAP user attribute name.
      - Required when type is 'ldap'.
    type: str
  user_classes:
    description:
      - The objectclasses for users. (default = C(inetorgperson, posixaccount, person, user))
    type: str
  username_claim:
    description:
      - OpenID claim used to generate the unique username.
    type: str
  verify:
    description:
      - Verify the server’s SSL certificate.
    type: bool
"""
### TODO: cf comment above
###  sync_attributes:
###    description:
###      - 'Comma separated list of key=value pairs for specifying which LDAP attributes map to which PVE user field.'
###      - 'For example, to map the LDAP attribute mail to PVEs email, write C(email=mail).'
###      - 'By default, each PVE user field is represented by an LDAP attribute of the same name.'
###    type: str
###  sync_defaults_options:
###    description:
###      - 'The default options for behavior of synchronizations.'
###      - 'C([enable-new=<1|0>] [,full=<1|0>] [,purge=<1|0>] [,remove-vanished=[acl];[properties];[entry]])'
###      - 'B(remove-vanished new since Proxmox 7.2)'
###      - 'B(full, purge deprecated since Proxmox 7.2)'
###    type: str
###   tfa_type:
###     description:
###       - Use Two-factor authentication.
###       - 'C(<TFATYPE> [,digits=<COUNT>] [,id=<ID>] [,key=<KEY>] [,step=<SECONDS>] [,url=<URL>])'
###     type: str

EXAMPLES = r"""
- name: Create Active Directory realm.
  julien_lecomte.proxmox.realm:
    name: corp
    type: ad
    domain: corp.example.com
    server1: corp.example.com
    default: true
    secure: true
    comment: "Example Active Directory (corp.example.com)"

- name: Sync Active Directory groups.
  julien_lecomte.proxmox.realm:
    name: corp
    sync: True
    sync_options:
      scope: "groups"
      full: False
      purge: False
"""

# TODO: List is incomplete
RETURN = r"""
name:
  description: Proxmox realmid.
  returned: always
  type: str
state:
  description: State ("absent" or "present").
  returned: always
  type: str
comment:
  description: Comment field.
  returned: when realm exists
  type: str
type:
  description: Realm type. (ad, ldap, openid, pam, pve)
  returned: when realm exists
  type: str
"""


class ProxmoxRealm(ProxmoxShell):
    def on_create_cmdline(self, cmd):
        cmd.extend(["-realm", self.params["name"]])
        return self.on_modify_cmdline(cmd)

    def on_modify_cmdline(self, cmd):
        # fmt: off
        for key in [
            "autocreate", "base_dn", "bind_dn", "capath", "cert", "certkey",
            "comment", "default", "domain", "filter", "group_classes", "group_dn",
            "group_filter", "group_name_attr", "mode", "password", "prompt", "scopes",
            "secure", "server1", "server2", "sslversion", "sync_attributes", "type",
            "user_attr", "user_classes", "username_claim", "verify",
        ]:
            cmd.extend(self.maybe_command(key))

        for key in [
            "client_id", "issuer_url", "acr_values", "case_sensitive",
            "client_key", "tfa_type", "sync_defaults_options",
        ]:
            cmd.extend(self.maybe_command(key, key.replace("_", "-")))
        # fmt: on
        return cmd


def main():
    module = ProxmoxRealm(
        argument_spec=dict(
            name=dict(type="str", required=True),
            state=dict(type="str", default="present", choices=["absent", "present"]),
            acr_values=dict(type="str"),
            autocreate=dict(type="bool"),
            base_dn=dict(type="str"),
            bind_dn=dict(type="str"),
            capath=dict(type="str"),
            case_sensitive=dict(type="str"),
            cert=dict(type="str"),
            certkey=dict(type="str"),
            client_id=dict(type="str"),
            client_key=dict(type="str"),
            comment=dict(type="str"),
            default=dict(type="bool"),
            domain=dict(type="str"),
            filter=dict(type="str"),
            group_classes=dict(type="str"),
            group_dn=dict(type="str"),
            group_filter=dict(type="str"),
            group_name_attr=dict(type="str"),
            issuer_url=dict(type="str"),
            mode=dict(type="str", choices=["ldap", "ldap+starttls", "ldaps"]),
            password=dict(type="str", no_log=True),
            port=dict(type="int"),
            prompt=dict(type="str"),
            scopes=dict(type="str"),
            secure=dict(type="bool"),
            server1=dict(type="str"),
            server2=dict(type="str"),
            sslversion=dict(type="str", choices=["tlsv1", "tlsv1_1", "tlsv1_2", "tlsv1_3"]),
            sync=dict(type="bool", default=False),
            sync_options=dict(
                type="dict",
                default={},
                options=dict(
                    dry_run=dict(type="bool", default=False),
                    enable_new=dict(type="bool"),
                    full=dict(type="bool"),
                    purge=dict(type="bool"),
                    remove_vanished=dict(type="str"),
                    scope=dict(type="str", choices=["both", "groups", "users"]),
                ),
            ),
            # sync_attributes=dict(type="str"),
            # sync_defaults_options=dict(type="str"),
            # tfa_type=dict(type="str"),
            type=dict(type="str", choices=["ad", "ldap", "openid", "pam", "pve"]),
            user_attr=dict(type="str"),
            user_classes=dict(type="str"),
            username_claim=dict(type="str"),
            verify=dict(type="bool"),
        ),
        supports_check_mode=False,
    )
    module.params["realm"] = module.params["name"]
    module.run(f"/access/domains/{module.params['name']}", "/access/domains")

    # Handle sync of AD and LDAP
    if module.data.get("state") == "present" and module.params["sync"] and module.data.get("type") in ["ad", "ldap"]:
        # save value for later
        changed = module.changed

        cmd = ["pveum", "realm", "sync", module.params["name"]]
        options = module.params.get("sync_options")
        for key in ["full", "purge", "scope", "dry_run", "enable_new", "remove_vanished"]:
            value = options.get(key)
            if value in [False, True]:
                value = "1" if value else "0"
            if value:
                key = key.replace("_", "-")
                cmd.extend([f"--{key}", value])

        # result code on failure is 0.
        module.execute_command(cmd)
        if options.get("dry_run", 0):
            module.changed = changed
        else:
            module.changed |= changed

    module.exit_json(changed=module.changed, warnings=module.warnings, realm=module.data)


if __name__ == "__main__":
    main()
