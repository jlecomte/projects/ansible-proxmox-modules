#!/usr/bin/env python3
# Copyright (C) 2022 Julien Lecomte <julien@lecomte.at>
# SPDX-License-Identifier: GPL-3.0-only


from ansible_collections.julien_lecomte.proxmox.plugins.module_utils.pvesh import ProxmoxShell

# FIXME: "password" is unsupported
#
DOCUMENTATION = r"""
---
module: user
plugin_type: module
author: Julien Lecomte (julien@lecomte.at)
short_description: Adds, modifies, or removes a Proxmox user.
description:
  - Adds, modifies, or removes a Proxmox user.
  - Returned values will exist in a variable named 'user'.
seealso:
  - name: pveum man page
    description: pveum - Proxmox VE User Manager
    link: https://pve.proxmox.com/pve-docs/pveum.1.html
options:
  name:
    description:
      - 'The user name with realm name (eg: example@pam).'
    type: str
    required: true
  state:
    description:
      - Specify if the user should exist (present) or absent.
    type: str
    choices: [ absent, present ]
    default: present
  comment:
    description:
      - Optionally sets the comment field.
    type: str
  email:
    description:
      - Optionally sets the email field.
    type: str
  enabled:
    description:
      - Enable the account (default on creation). You can set this to False to disable the account.
    type: bool
  expire:
    description:
      - Account expiration date (seconds since epoch). 0 means no expiration date (default on creation).
    type: int
  firstname:
    description:
      - Optionally sets the firstname field.
    type: str
  groups:
    description:
      - Add user to specified groups.
    type: list
    elements: str
  key_ids:
    description:
      - Keys for two factor auth (yubico).
    type: str
  lastname:
    description:
      - Optionally sets the lastname field.
    type: str
"""

EXAMPLES = r"""
- name: Create user 'example'
  julien_lecomte.proxmox.user:
    name: 'example@pam'

- name: Create user 'example' and add to group 'mygroup'
  julien_lecomte.proxmox.user:
    name: 'example@pam'
    groups: [ "mygroup" ]
"""

RETURN = r"""
name:
  description: Proxmox user id.
  returned: always
  type: str
state:
  description: State ("absent" or "present").
  returned: always
  type: str
realm_type:
  description: Proxmox realm type.
  returned: when user exists
  type: str
comment:
  description: Comment field.
  returned: when user exists
  type: str
email:
  description: Email field.
  returned: when user exists
  type: str
enabled:
  description: Enable field.
  returned: when user exists
  type: int
expire:
  description: Account expiration date (seconds since epoch).
  returned: when user exists
  type: int
firstname:
  description: Firstname field.
  returned: when user exists
  type: str
groups:
  description: List of groups.
  returned: when user exists
  type: list
  elements: str
key_ids:
  description: Keys for two factor auth (yubico).
  returned: when user exists
  type: str
lastname:
  description: Lastname field.
  returned: when user exists
  type: str
"""


class ProxmoxUser(ProxmoxShell):
    def on_create_cmdline(self, cmd):
        cmd.extend(["-userid", self.params["name"]])
        return self.on_modify_cmdline(cmd)

    def on_modify_cmdline(self, cmd):
        if self.params.get("expire") and self.params["expire"] < 0:
            self.fail_json(msg="expire must be 0 or a positive integer")

        for key in [
            "comment",
            "email",
            "expire",
            "firstname",
            "lastname",
        ]:
            cmd.extend(self.maybe_command(key))

        cmd.extend(self.maybe_command("enabled", "enable"))
        cmd.extend(self.maybe_command("key_ids", "keys", "key_ids"))

        groups = self.params["groups"]
        if groups is not None and sorted(self.data.get("groups", [])) != sorted(groups):
            cmd.extend(["-groups", ",".join(groups)])

        return cmd


def main():
    module = ProxmoxUser(
        argument_spec=dict(
            name=dict(type="str", required=True),
            state=dict(type="str", default="present", choices=["absent", "present"]),
            comment=dict(type="str"),
            email=dict(type="str"),
            enabled=dict(type="bool"),
            expire=dict(type="int"),
            firstname=dict(type="str"),
            groups=dict(type="list", elements="str"),
            key_ids=dict(type="str"),
            lastname=dict(type="str"),
            # password=dict(type="str", no_log=True),
        ),
        supports_check_mode=False,
    )
    module.params["userid"] = module.params["name"]
    module.run(f"/access/users/{module.params['name']}", "/access/users")
    module.exit_json(changed=module.changed, warnings=module.warnings, user=module.data)


if __name__ == "__main__":
    main()
